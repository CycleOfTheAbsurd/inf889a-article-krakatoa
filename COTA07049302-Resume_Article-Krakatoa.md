---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=2cm
fontfamily:
- charter
---
\begin{titlepage}
\begin{center}
    \vspace*{2cm}

    \textbf{Analyse de programmes pour la sécurité logicielle}

        INF889A

    \vspace{3cm}

    \textbf{Alexandre Côté Cyr - COTA07049302}

    \vspace{3cm}

        Présentation d'un article\\
        \textbf{Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?) (1997)}

    \vspace{3cm}

        Travail remis\\
        à\\
        Jean Privat

    \vfill

    Université du Québec à Montréal\\
    24 Mars 2020

    \vspace*{2cm}
\end{center}
\end{titlepage}

\thispagestyle{empty}
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}

# Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?) (1997)

L'article décrit une manière pour récupérer du code source Java compréhensible par un humain à partir de _bytecode_ compilé. Il se concentre sur deux sous-aspects de cette problématique: la récupération d'expressions Java et la synthétisation du flux de contrôle à l'aide de blocs de haut niveau (_i.e._ boucles et blocs conditionnels).

Trois techniques sont proposées pour arriver à résoudre ces problèmes:
 - L'exécution symbolique du _bytecode_ en simulant la pile d'exécution. Ceci sert à récupérer les expressions de haut niveau.
 - L'élimination des _GOTO_ via une version modifiée de l'algorithme de Ramshaw[^1]. Cette technique remplace les sauts inconditionnels par des boucles `for` contenant des `continue` et des `breaks` aux bons endroits afin d'obtenir un flux de contrôle valide en Java.
 - La restructuration du flux de contrôle via l'application de règles de réécriture sur l'AST. Cette étape utilise le _Controlflow Analysis_ afin de transformer les boucles imbriquées précédemment obtenues en des blocs de contrôle appropriés. Ceci sert à obtenir un flux de contrôle plus clair et idiomatique.

La troisième étape présente la contribution la plus importante de l'article au domaine de la décompilation. L'algorithme de restructuration et les règles qui le composent sont cités jusqu'à aujourd'hui dans des publications sur le sujet. L'amélioration de l'algorithme de Ramshaw faite dans l'article est simple, mais augmente l'utilité de ses résultats pour des transformations successives.

Quelques exemples d'entrées et des sorties résultantes sont présentées, mais l'outil décrit dans l'article n'a jamais été mis à la disposition du public, il est donc difficile de tester sa validité et des faiblesses.
Certaines limites de l'approche sont présentées dans l'article:

 - L'introduction de _threads_ d'exécution peut nuire à la correctitude de l'exécution symbolique.
 - L'algorithme de restucturation peut être déjoué par des sauts conditionnels qui ne seront jamais exécuté.
 - Les expressions booléennes récupérées ne sont pas simplifiées et utilisent des comparaisons d'entiers plutôt que de booléens.

Les deux premières sont surtout importantes dans un contexte adversarial où le code compilé est volontairement obfusqué.

La validation de l'algorithme de Ramshaw peut être trouvée dans l'article original[^2]. La restructuration du flux de contrôle est garantie de se terminer, car chaque application d'une règle réduit la taille de l'AST.

[^1]: Ramshaw, Lyle. 1988. Eliminating go to’s while preserving program structure. J. ACM 35, 4 (October 1988), 893–920. DOI:[https://doi.org/10.1145/48014.48021](https://doi.org/10.1145/48014.48021)
[^2]: _ibid_
