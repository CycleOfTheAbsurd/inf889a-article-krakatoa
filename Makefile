slides:=dist/COTA07049302-Slides_Article-Krakatoa.pdf
document:=dist/COTA07049302-Document_Article-Krakatoa.pdf
resume:=dist/COTA07049302-Resume_Article-Krakatoa.pdf

all: $(slides) $(document) $(resume)

$(slides): $(notdir $(slides:.pdf=.md))
	pandoc $< -t beamer -o $@

$(document): $(notdir $(document:.pdf=.md))
	pandoc $< -o $@

$(resume): $(notdir $(resume:.pdf=.md))
	pandoc $< -o $@

clean:
	rm -rf dist/*
