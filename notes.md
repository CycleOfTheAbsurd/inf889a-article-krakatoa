The JVM requires a signifcant amount of type information from the classes for object linking. Furthermore, the bytecode must be veri ably well-behaved in order to ensure safe execution. Decompilation systems can exploit this type information and well-behaved property to recover Java source code from the class file.

# Techniques

Type inference
stack-simulation technique
extension of Ramshaw's goto-elimination algorithm
code rewrite rules
simplification de l'AST
Traverse de l'AST pour récupérer le code source

Symbolic execution of the bytecode creates the corresponding pseudo-Java source expressions. It also creates conditional and unconditional goto's, which will be removed by subsequent decompilation steps.
	GOTO est pas valide en Java

Ramshaw presented an algorithm for eliminating
goto's from Pascal programs while preserving the
program's structure [Ram88]. This algorithm re-
places each goto with a multilevel break to a sur-
rounding loop. The algorithm determines the ap-
propropriate locations for these surrounding loops.
We trivially extended his algorithm to use multi-
level continue's.

To utilize Ramshaw's algorithm, we developed an
algorithm that orders a reducible graph's instruc-
tions such that the resulting augmented graph is
also reducible.

Krakatoa then proceeds to
recover more of the natural high-level constructs
of the original program (e.g. if-then-else, etc.).
Krakatoa uses a program point analysis to summa-
rize a program's control- ow and to guide recover-
ing high-level constructs.

Each rewriting rule reduces the size of
the AST, thus ensuring termination.
VOIR TABLEAU DES RÈGLES

## Problèmes couverts dans le papier:

 - recovering source-level expressions
 - synthesizing high-level control constructs from goto-like primitives.

# Différences avec les autres decompiler

Utilisents souvent _graph transformation_ pour récupérer le control-flow
	Doit anticiper tous les idiomes possible de contrôle de flux, sinon incapable de les traiter
Krakatoa récupère d'abord du Java valide, puis reconstruit le flux

# Améliorations possibles/Faiblesses

using DeMorgan's laws would simplify the boolean expressions. Future versions of Krakatoa will do so.

Incapable de récupérer les expressions ternaireso

One could introduce irreducible control- ow
through bogus conditional jumps to foil Ramshaw's
algorithm. This, however, only stops the recre-
ation of high-level constructs. Krakatoa could sim-
ply produce source code in a Java-like language ex-
tended with goto's.
One could introduce bizarre stack behavior to foil

It is possible, however, to
create many bogus threads of control (i.e., threads
that will never execute) that will confuse the ex-
pression recovery mechanism in basic blocks that
are entered with non-empty stacks.
One code obfuscation technique that is modestly

One code obfuscation technique that is modestly
e ective is to change the class le's symbol table to
contain bizarre names for elds and methods. So
long as cooperating classes agree on these names,
the class les will link and execute correctly [vV96,
Sri96].

If Krakatoa is presented with
a high-level language idiom that it does not rec-
ognize, it may leave unnecessary breaks or con-
tinues in the code. It will still produce legal Java,
however.

VOIR TABLEAU RÈGLES ADDITIONNELLES

