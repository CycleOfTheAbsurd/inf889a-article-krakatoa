---
papersize:
- a4
fontsize:
- 12pt
geometry:
- margin=2cm
fontfamily:
- charter
---
\begin{titlepage}
\begin{center}
    \vspace*{2cm}

    \textbf{Analyse de programmes pour la sécurité logicielle}

        INF889A

    \vspace{3cm}

    \textbf{Alexandre Côté Cyr - COTA07049302}

    \vspace{3cm}

        Présentation d'un article\\
        \textbf{Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?) (1997)}

    \vspace{3cm}

        Travail remis\\
        à\\
        Jean Privat

    \vfill

    Université du Québec à Montréal\\
    24 Mars 2020

    \vspace*{2cm}
\end{center}
\end{titlepage}

\thispagestyle{empty}
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}

# Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?) (1997)

Cet article présente l'outil Krakatoa, un décompilateur pour Java, ainsi que les techniques que l'outil met en oeuvre.

Les deux problèmes principaux que les auteurs cherchent à résoudre sont les suivants:
 - La récupération d'expressions en langage source. Il s'agit de retrouver les expressions telles qu'écrites par le programmeur en se basant sur la sémantique des instructions de bas-niveau qui sont générées à la compilation.
 - La synthétisation de blocs de haut niveau et de flux de contrôle à partir de primitives `goto`. Dans le _bytecode_ Java, comme dans la plupart des langages de bas niveau, le contrôle du flux d'exécution se fait à l'aide de sauts conditionnels ou inconditionnels. Ceux-ci sont généralement représentés par des instructions de type `jump` ou `goto`. Par contre, ces instructions ne représentent pas bien la sémantique des boucles et blocs conditionnels du langage de haut niveau. Il est donc intéressant de les reconstruire pour aider la compréhension et l'analyse du code.

## Particularités de la décompilation Java

Java est un langage qui est compilé vers du _bytecode_, une représentation intermédiaire qui pourra ensuite être exécutée par la machine virtuelle Java. En comparaison avec la compilation vers du code machine, la compilation vers du _bytecode_ conserve beaucoup plus d'information du code source original.[^1]
Ceci inclue des métadonnées sur les noms de classes, de méthodes ainsi que sur les types des champs et des paramètre de méthodes. De plus, le compilateur Java effectue moins d'optimisations qu'un compilateur vers du langage machine. L'optimisation des instructions est plutôt faite par le JIT[^2] au moment de l'exécution du programmes. La sortie est donc plus près du code source original ce qui rend sa compréhension plus simple.

De plus, le vérificateur de la JVM effectue plusieurs vérifications sur le format et le comportement des fichiers compilés. Tout fichier qui ne passe pas ces vérifications est rejeté et considéré comme invalide.[^3] Le décompilateur peut donc se servir du fait que le programme doit se comporter de façon définie et être valide pour son analyse. Il peut aussi utiliser certaines techniques et informations dont se sert le vérificateur pour obtenir certaines propriétés du programme.

Pour ces raisons, la décompilation de _bytecode_ est généralement plus facile et produit un résultat plus fidèle que la décompilation de binaire.

[^1]: Lindholm, Tim; Frank Yellin, Gilad Bracha, Alex Buckley, & Daniel Smith. 2019. Chapter 4. The class File Format. The Java Virtual Machine Specification - Java SE 13 Edition. (August 2019). Retrieved March 1, 2020 from https://docs.oracle.com/javase/specs/jvms/se13/html/jvms-4.html
[^2]: Evans, Ben. 2014. Understanding Java JIT Compilation with JITWatch, Part 1. Oracle. (July 2014). https://www.oracle.com/technical-resources/articles/java/architect-evans-pt1.html
[^3]: Venners, Bill. 1997. Security and the class verifier. JavaWorld. (October 1997). https://www.javaworld.com/article/2077038/security-and-the-class-verifier.html?page=2

## Techniques utilisées

L'article présente plusieurs techniques utilisées par l'outil pour récupérer du code source Java valide à partir du bytecode. Celles-ci sont appliquées consécutivement et, à chaque étape, la sortie se rapproche de code source java idiomatique.

Krakatoa effectue de l'inférence de type en utilisant les mêmes techniques que le vérificateur de la JVM et se sert de ces informations dans les autres phases de son analyse.

### Exécution symbolique du bytecode

Cette étape a pour but de récupérer des expressions de haut niveau à partir des opérations dans le _bytecode_. Krakatoa simule l'exécution du programme en utilisant une pile où sont placées des représentations des iexpressions résultant de l'exécution des intructions. Cette simulation s'effectue en reconstruisant un bloc du programme à la fois.

![Figure 3](figure3.png){ width=650px }[^4]

Après cette étape de l'analyse, nous obtenons donc un programme en pseudo-Java où le contrôle du flux de programme se fait avec des instructions `GOTO`

[^4]: Proebsting, Todd A. & Scott A. Watterson. [Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?)](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.5640). COOTS'97. DOI:https://doi.org/10.1.1.39.5640

### Élimination des `GOTO` et restructuration du flux

Java ne possède pas d'instruction `GOTO`. De plus, les `GOTO` représente moins clairement le flux de contrôle du programme que des boucles et blocs conditionnels. Les problèmes d'intelligibilité liés à l'utilisation de `GOTO` sont reconnus depuis au moins 1968[^5]. Il est donc naturel de vouloir les éliminer dans un cadre d'analyse de programme.

Pour ce faire, Krakatoa utilise une variation de l'algorithme de Ramshaw[^6]. Cet algorithme remplace les `GOTO` par des boucles contenant des `break` et `continue`. Un saut vers l'avant est remplacé par une boucle qui se termine juste avant la cible et qui un `break` qui peut être précédé, le cas échéant, par la condition pour effectuer le saut. De manière semblable, un saut vers l'arrière est remplacé par une boucle commençant juste avant la cible et un `continue`. Chaque boucle se termine par un `break` pour maintenir le flux d'exécution du programmea.
En appliquant successivement ces transformations, on obtient un programme composé de boucles imbriquée qui reproduit exactement le flux d'exécution du programme avec les `GOTO`.

À la fin de cette phase, le résultat est donc un programme Java valide. Le flux de contrôle est peu intuitif en raison du grand niveau d'imbrication et de l'utilisation de boucles au lieu de blocs conditionnels. Par contre, cette structure met en place les éléments nécessaires à la dernière phase de l'analyse.

![Figure 4](figure4.png){ width=550px }[^7]

[^5]: Dijkstra, Edsger W. 1968. Letters to the editor: go to statement considered harmful. Commun. ACM 11, 3 (March 1968), 147–148. DOI:https://doi.org/10.1145/362929.362947
[^6]: Ramshaw, Lyle. 1988. Eliminating go to’s while preserving program structure. J. ACM 35, 4 (October 1988), 893–920. DOI:[https://doi.org/10.1145/48014.48021](https://doi.org/10.1145/48014.48021)
[^7]: Proebsting. _op. cit._

### Règles de réécriture

Afin d'obtenir un AST plus simple et un flux de contrôle plus idiomatique, Krakatoa met en oeuvre plusieurs règles de réécriture. Pour ce faire, l'outil utilise l'analyse des points de programme. Ce type d'analyse est une forme de _Controlflow Analysis_; car elle tient compte du flux du programme et utilise des points placés avant et après chaque instruction. Ceci diffère du `Dataflow Analysis`, car on ne s'intéresse qu'aux chemins d'exécution, pas à l'état du programme en suivant ces chemins. Dans notre cas, l'analyse ne s'intéresse qu'à la possibilité d'atteindre les points et à l'équivalence entre différents points.

Un point est inatteignable si, dans tous les chemins possibles, il est précédé par un saut inconditionnel. L'équivalence entre plusieurs points se définit comme suit: deux points sont équivalents s'il n'y a aucune différence entre les exécutions du programme à partir de ces deux points. (Par exemple, le point après un `break` et le point à la fin de la boucle contenant ce `break`.)

À partir de ces caractéristiques, des transformations sont appliquées à l'AST. Celles-ci visent entre-autres à éliminer les blocs vides et instructions inutiles, à combiner les points de programme équivalents et à transformer les boucles ne se répétant pas en blocs conditionnels. Ces transformations sont appliquées successivement en suivant les règles définies jusqu'à ce que leur application ne modifie plus la structure du programme. Chacune de ces règles diminue la taille de l'AST, il est donc garanti que leur application successive se termine.

Voir [l'annexe 1](#annexe-1) pour la liste des règles mises en place par l'outil.


## Comparaison avec d'autres décompilateurs

Certains autres décompilateurs Java font une association entre des motifs dans le graphe de flux de contrôle et des constructions dans le langage source. Ceci a pour avantage de produire du code très idiomatique et clair pour ces constructions. Par contre, chacune de celles-ci doit être définie par le programmeur et le décompilateur peut produire du code étrange ou bien causer des erreurs lorsqu'il rencontre une construction qui n'a pas été définie précedemment.[^8]

Certaines techniques de décompilation débutent par la restructuration du graphes de flux de contrôle pour ensuite extraire les constructions en langage source. Ceci a pour effet que, si à un moment de l'analyse, le décompilateur n'arrive pas à récupérer une construction de haut niveau, cette section de code risque de ne pas être décompilée.[^9] Au contraire, Krakatoa reconstitue d'abord du code source valide avant de le restructurer. Donc, dans l'éventualité d'un problème lors de l'analyse, nous obtiendrons tout de même du code Java correct même si sa structure n'est pas idiomatique.

[^8]: Cifuentes, Cristina. 1993. [A Structuring Algorithm for Decompilation.](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.40.3657). XIX Conferencia Latinoamericana de Inform'atica. DOI:10.1.1.40.3657
[^9]: Lichtblau, Ulrike. 1985. Decompilation of Control Structures by Means of Graph Transformations. Lecture Notes in Computer Science No.185. 284-297. 10.1007/3-540-15198-2_18.


## Lacunes et améliorations possibles

L'article ne traite pas de la manière dont Krakatoa gère les classes, l'héritage et les structures de données, donc il n'est pas possible de décrire des lacunes en lien avec ces aspects. Par contre, la décompilation du flux d'exécution peut être déjouée de différentes manières. La plupart de celles-ci nécessitent une obfuscation volontaire du fichier à décompiler par son auteur. Ces cas sont tout de même intéressants à considérer, car la décompilation d'un programme peut être une situation adversariale, par exemple, un auteur de _malware_ qui veut complexifier l'analyse de celui-ci ou une compagnie produisant un logiciel propriétaire qui désire protéger sa propriété intellectuelle.

Il est possible d'ajouter des sauts conditionnels qui ne seront jamais exécutés. Ceci peut nuire à l'élimination des `GOTO` et donc générer du code Java invalide (`GOTO` plutôt que boucles et blocs conditionnels) et moins clair. Des _threads_ d'exécution bidon peuvent aussi être introduits. Ceci rend rend plus difficile l'exécution symbolique du programme à travers une simulation de la pile d'exécution, car la pile simulée sera partagée entre les _threads_. La récupération d'expressions en Java à partir du _bytecode_ est donc rendue moins efficace, voire incomplète.

L'article n'amène pas de solutions à ces deux problèmes. Au vu de leur nature, il est possible que leur résolution nécessite des changements importants dans la méthode utilisée par l'outil pour décompiler.

En observant les exemples de décompilation fournie, on peut remarquer que l'outil a de la difficulté avec la manipulation de booléens. On peut observer des cas de double négation ou de comparaisons avec des valeurs entières qui, bien que valide, nuisent à la compréhension du code. Une simplification des expressions booléennes pourrait être effectuée en suivant des règles liées à leur sémantique. Comme le suggèrent les auteurs, ceci pourrait être accompli en appliquant les lois de De Morgan sur l'équivalence entre les propositions logiques.

Les auteur proposent deux nouvelles règles de réécriture qui affectent les boucles `for`. Celles-ci ont pour but d'inclure l'initialisation et la mise à jour d'une variable dans la déclaration de la boucle.


\pagebreak
# Bibliographie

Cifuentes, Cristina. 1993. [A Structuring Algorithm for Decompilation.](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.40.3657). XIX Conferencia Latinoamericana de Inform'atica. DOI:10.1.1.40.3657

Dijkstra, Edsger W. 1968. Letters to the editor: go to statement considered harmful. Commun. ACM 11, 3 (March 1968), 147–148. DOI:https://doi.org/10.1145/362929.362947

Erosa, Ana M. & Laurie J. Hendren. 1994. [Taming control flow: A structured approach to eliminating goto statements.](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.54.9516). ICCL'94. DOI:10.1.1.54.9516

Evans, Ben. 2014. Understanding Java JIT Compilation with JITWatch, Part 1. Oracle. (July 2014). https://www.oracle.com/technical-resources/articles/java/architect-evans-pt1.html

Lichtblau, Ulrike. 1985. Decompilation of Control Structures by Means of Graph Transformations. Lecture Notes in Computer Science No.185. 284-297. 10.1007/3-540-15198-2_18.

Lindholm, Tim; Frank Yellin, Gilad Bracha, Alex Buckley, & Daniel Smith. 2019. Chapter 4. The class File Format. The Java Virtual Machine Specification - Java SE 13 Edition. (August 2019). Retrieved March 1, 2020 from https://docs.oracle.com/javase/specs/jvms/se13/html/jvms-4.html

Proebsting, Todd A. & Scott A. Watterson. 1997. [Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?)](https://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.5640). COOTS'97. DOI:10.1.1.39.5640

Ramshaw, Lyle. 1988. Eliminating go to’s while preserving program structure. J. ACM 35, 4 (October 1988), 893–920. DOI:[https://doi.org/10.1145/48014.48021](https://doi.org/10.1145/48014.48021)

Venners, Bill. 1997. Security and the class verifier. JavaWorld. (October 1997). https://www.javaworld.com/article/2077038/security-and-the-class-verifier.html?page=2

\pagebreak

# Annexe 1
## Règles de transformation

![Table 1](table1.png){ width=710px } (Proebsting 1997)
