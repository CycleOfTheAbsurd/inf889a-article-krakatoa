\title{Krakatoa: Decompilation in Java (Does Bytecode Reveal Source?) (1997) by Todd A. Proebsting , Scott A. Watterson}
\author{Alexandre Côté Cyr}
\institute{UQAM - INF889A}
\maketitle

# Problématique

Récupérer du code source Java compréhensible à partir du _Bytecode_ compilé.

Le _bytecode_ a des `GOTO`, mais pas Java.

`GOTO`s nuisent à la compréhension du code.[^1]

[^1]: Dijkstra, Edsger W. 1968. Letters to the editor: go to statement considered harmful. Commun. ACM 11, 3 (March 1968), 147–148. DOI:https://doi.org/10.1145/362929.362947

# Contexte de publication

L'article date de 1997 et a été présenté dans le cadre de la 3è édition de Usenix COOTS (_Conference on Object-Oriented Technologies_) .

3 ans après la publication de _Reverse Compilation Techniques_ la thèse doctorale de Christina Cifuentes qui est considéré comme l'une des publications les plus importantes dans le domaine la la décompilation.

1 an après la première version publique de Java (JDK 1.0: Janvier 1996)

Autres décompilateurs Java de l'époque: Mocha[^2], WingDis, DeJava/DejaVu[^3]

[^2]: http://www.brouhaha.com/~eric/software/mocha/
[^3]: https://www.javaworld.com/article/2076968/java-decompilers-compared.html

# Auteurs

Tous les deux sont chercheurs au département d'informatique de l'Université de l'Arizona.

__Todd A. Proebsting__

> My primary focus investigating how to get rid of all the clutter in most statically-checked computer programs without sacrificing the benefits of static analysis.

Domaines d'intérêt: analyse statique, compilateurs, machines virtuelles

À l'époque, professeur associé, aujourd'hui Professeur et directeur du département.

__Scott Watterson__

Domaines d'intérêt: compilateurs, optimisation de code

# Caractéristiques de la décompilation Java

 - Compilation vers du _bytecode_ préserve plus de métadonnées que la compilation vers du code machine.[^4]
 	- Classes, types, signatures de méthode
 - Compilateur Java optimise moins, l'optimisation est laissée au JIT[^5]
 - Le _bytecode_ Java doit être approuvé par le vérificateur de la JVM pour être valide.[^6]


[^4]: Lindholm, Tim; Frank Yellin, Gilad Bracha, Alex Buckley, & Daniel Smith. 2019. Chapter 4. The class File Format. The Java Virtual Machine Specification - Java SE 13 Edition. (August 2019). Retrieved March 1, 2020 from https://docs.oracle.com/javase/specs/jvms/se13/html/jvms-4.html
[^5]: Evans, Ben. 2014. Understanding Java JIT Compilation with JITWatch, Part 1. Oracle. (July 2014). https://www.oracle.com/technical-resources/articles/java/architect-evans-pt1.html
[^6]: Venners, Bill. 1997. Security and the class verifier. JavaWorld. (October 1997). https://www.javaworld.com/article/2077038/security-and-the-class-verifier.html?page=2


# Objectifs de la publication

 - Récupération d'expressions en langage source
 - Synthétisation de blocs de haut niveau et de flux de contrôle


# Solutions proposées

## Récupération des expressions en langage source

Exécution symbolique du _bytecode_ avec une simulation de pile d'exécution

![Figure 3](figure3.png){ width=450px }.

---

## Élimination des GOTO

Restructuration du flux avec une version modifiée l'algorithme de Ramshaw[^7]

![Figure 4](figure4.png){ width=450px }.

[^7]: Ramshaw, Lyle. 1988. Eliminating go to’s while preserving program structure. J. ACM 35, 4 (October 1988), 893–920. DOI:[https://doi.org/10.1145/48014.48021](https://doi.org/10.1145/48014.48021)

---

## Récupération du flux de contrôle

Règles de réécriture sur l'AST
 - _Controlflow analysis_
 - Analyse des points de programme
 - Chaque application réduit la taille de l'AST

---

![Table 1](table1.png){ width=500px }.

---


# Importance de l'article dans le domaine

Bien que l'article ait plus de 20 ans, plusieurs publications datant de la seconde moitié des années 2010 le citent encore.

## Contributions

 - Exécution symbolique du _bytecode_
 - Amélioration de l'algorithme de Ramshaw
 - Algorithme de restructuration


# Analyse de l'approche

### Forces

 - Pas besoin de définir les équivalences entre motifs en _bytecode_ et en Java.
 - Récupère d'abord du code Source en Java avec des `GOTO`s, donc plus robuste face aux erreurs
 - Restructuration peut être améliorée facilement par l'ajout de nouvelles règles


### Faiblesses

 - Restructuration peut être déjouée par des sauts conditionnels inutiles
 - Récupération d'expressions peut être rendue difficile par l'introduction de _threads_ bidons
 - Expressions booléennes récupérées souvent peu claires
 - L'outil n'a jamais été rendu disponible publiquement


# Exemples

[Voir les annexes de l'article](http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.39.5640)
